ARG DB_VERSION="8.0"

FROM library/mysql:${DB_VERSION}-debian

COPY config/my.cnf /etc/mysql/conf.d/custom.cnf

RUN apt update && \
    apt upgrade -y && \
    apt install -y python3-pip && \
    pip3 install --upgrade pip && \
    pip3 install mycli && \
    chmod 644 /etc/mysql/conf.d/custom.cnf

VOLUME [ "/var/lib/mysql" ]
