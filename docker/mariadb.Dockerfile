ARG DB_VERSION="10.7"

FROM library/mariadb:${DB_VERSION}

COPY config/my.cnf /etc/mysql/conf.d/custom.cnf
RUN apt update && \
    apt upgrade -y && \
    apt install -y python3-pip && \
    pip3 install --upgrade pip && \
    pip3 install mycli && \
    chmod 644 /etc/mysql/conf.d/custom.cnf

VOLUME [ "/var/lib/mysql" ]
