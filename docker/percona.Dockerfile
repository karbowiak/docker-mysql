ARG DB_VERSION="8.0"

FROM percona/percona-server:${DB_VERSION}

USER root

ARG DB_VERSION="8.0"

COPY config/my.cnf /etc/my.cnf.d/custom.cnf

RUN chmod 644 /etc/my.cnf.d/custom.cnf

VOLUME [ "/var/lib/mysql" ]
