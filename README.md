# docker-mysql

This repository contains multiple images for MySQL, Percona and MariaDB

# Database types:
- mysql
- mariadb
- percona

# Database versions
### MySQL:
- 8.0
- 5.7
### MariaDB
- 10.7
- 10.6
- 10.5
- 10.4
- 10.3
- 10.2
- 10.1
### Percona
- 8.0
- 5.7
- 5.6

Refer to the upstream docker repos documentation for more information on how to use them
- MySQL: https://hub.docker.com/_/mysql
- Percona: https://hub.docker.com/r/percona/percona-server
- MariaDB: https://hub.docker.com/_/mariadb
